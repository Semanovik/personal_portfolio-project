from django.shortcuts import render
from .models import Project

def home(request):
    projects = Project.objects.all()
    return render(request, 'portfolio/home.html', {'projects':projects})

def info(request):
    return render(request, 'portfolio/info.html')


# Create your views here.
